package com.besaba.okomaru_server.junT58.WaterDamage;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class Main extends JavaPlugin implements Listener{

	public void onEnable(){
		getServer().getPluginManager().registerEvents(this, this);
	}

	@EventHandler
	public void onMove(PlayerMoveEvent event){
		final Player player = event.getPlayer();
		if(!player.isDead() && !player.getGameMode().equals(GameMode.CREATIVE)){
			if(player.getLocation().getBlock().getType().equals(Material.WATER) || player.getLocation().getBlock().getType().equals(Material.STATIONARY_WATER)){
				final int task = getConfig().getInt(player.getName() + ".Task");
				if(task == 0){
					getConfig().set(player.getName() + ".Task", 1);
					saveConfig();
					new BukkitRunnable(){
						public void run(){
							if(player.getLocation().getBlock().getType().equals(Material.WATER) || player.getLocation().getBlock().getType().equals(Material.STATIONARY_WATER)){
								player.damage(1);
								if(player.getHealth() <= 0){
									getConfig().set(player.getName() + ".Task", 0);
									saveConfig();
								}
								int task = getConfig().getInt(player.getName() + ".Task");
								if(task == 0){
									this.cancel();
								}
							}else{
								this.cancel();
							}
						}
					}.runTaskTimer(this, 20*10, 20);
				}
			}else{
				getConfig().set(player.getName() + ".Task", 0);
				saveConfig();
			}
		}
	}

}
